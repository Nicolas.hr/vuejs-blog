import axios from 'axios'
import { ref } from 'vue'

const getPosts = () => {
  const posts = ref([])
  const error = ref(null)

  const load = async () => {
    try {
      const response = await axios.get('http://localhost:3000/posts')

      if (response.status !== 200) {
        throw Error('No data available')
      }

      console.log(response.data)

      posts.value = await response.data
    } catch (err) {
      error.value = err.response
    }
  }

  return { posts, error, load }
}

export default getPosts
