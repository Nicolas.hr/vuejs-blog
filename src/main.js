import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './assets/main.css'

const app = createApp(App)

app.use(router)

// create a custom command: v-focus
app.directive('focus', {
  mounted: (el) => el.focus()
})

app.mount('#app')
