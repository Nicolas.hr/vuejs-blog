import { mount } from 'cypress/vue'
import SinglePost from './SinglePost.vue'

describe('<SinglePost />', () => {
  const post = {
    id: 1,
    title: 'Boost your conversion rate',
    href: '#',
    description:
      'Illo sint voluptas. Error voluptates culpa eligendi. Hic vel totam vitae illo. Non aliquid explicabo necessitatibus unde. Sed exercitationem placeat consectetur nulla deserunt vel. Iusto corrupti dicta.',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Marketing', href: '#' },
    author: {
      name: 'Michael Foster',
      role: 'Co-Founder / CTO',
      href: '#',
      imageUrl:
        'https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80'
    }
  }

  it('renders', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(SinglePost, { propsData: { post: post } })
    cy.get('time').should('have.text', post.date)
    cy.get('[data-testid=categoryTitle]').should('have.text', post.category.title)
    cy.get('[data-testid=postTitle]').should('have.text', post.title)
    cy.get('[data-testid=description]').should('have.text', post.description)
    cy.get('[alt="authorPicture"]').should('be.visible')
    cy.get('[data-testid=authorName]').should('have.text', post.author.name)
    cy.get('[data-testid=authorRole]').should('have.text', post.author.role)
    cy.get('[data-testid=btnPostDelete]').should('have.text', 'Delete')
    cy.get('[data-testid=btnEdit]').should('have.text', 'Edit')
    cy.get('[data-testid=deleteModalContainer]').should('not.be.visible')
  })

  it('check that delete modal is displayed after dlete button click', () => {
    cy.mount(SinglePost, { propsData: { post: post } })
      .get('[data-testid=btnPostDelete]')
      .click()
    cy.get('[data-testid=deleteModalContainer]').should('be.visible')
  })
})
