import DeleteModal from './DeleteModal.vue'
import { mount } from 'cypress/vue'

describe('<DeleteModal />', () => {
  it('mounts', () => {
    mount(DeleteModal, { props: { show: true } })
  })

  it('renders', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(DeleteModal, { props: { show: true } })
    cy.get('h3').should('have.text', 'Delete post')
    cy.get('p').should('have.text', 'Are you sure you want to delete this post?')

    cy.get('[data-testid=btnModalDelete]').should('have.text', 'Delete')
    cy.get('[data-testid=btnCancel]').should('have.text', 'Cancel')
  })

  it('isnt displayed when show is false', () => {
    mount(DeleteModal, { props: { show: false } })
      .get('[data-testid=deleteModalContainer]')
      .should('not.be.visible')
  })

  it('is displayed when show is true', () => {
    mount(DeleteModal, { props: { show: true } })
      .get('[data-testid=deleteModalContainer]')
      .should('be.visible')
  })

  it('emits a "delete" event once when delete button is clicked', () => {
    mount(DeleteModal, { props: { show: true } })
      .get('[data-testid=btnModalDelete]')
      .click()
      .vue()
      .then((wrapper) => {
        expect(wrapper.emitted('delete')).to.have.length(1)
      })
  })

  it('emits a "close" event once when cancel button is clicked', () => {
    mount(DeleteModal, { props: { show: true } })
      .get('[data-testid=btnCancel]')
      .click()
      .vue()
      .then((wrapper) => {
        expect(wrapper.emitted('close')).to.have.length(1)
      })
  })
})
